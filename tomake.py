import os
import os.path
import platform

# ============================================================================
# Global variables
# ============================================================================

test_name = 'test.exe'

start_dir = '.'
file_list = []

cpp_ext = '.cpp'
h_ext   = '.h'

headers = []
if platform.system() == 'Windows':
	headers_path = ['C:\\MinGW\\include',
	                'C:\\MinGW\\lib\\gcc\\mingw32\\6.3.0\\include',
	                'C:\\SFML-2.5.0\\include']
	dir_headers_path = ['C:\\MinGW\\lib\\gcc\\mingw32\\6.3.0\\include\\c++']
else:
	# TODO: change path for Linux system.
	headers_path = ['/usr/local/include']
	dir_headers_path = ['']

makefile_text = ' \
# Generated automatically \n\n\
CXX      = g++ \n\
CXXFLAGS = -std=c++14 -g -Wall -I.\n\
LDFLAGS  = \n\
LIBS     = \n\
\n\
target = ' + test_name + '\n\
\n\
.PHONY : all\n\
all : makedirs $(target) \n\
\n'

objects_dir = '../objects/'

sources_rule = 'sources = $($(all_objects:../%=%):%.o=%.cpp)\n'

compile_text    = '$(CXX) $(CXXFLAGS) $< -c -o $@\n'
target_dep_text = '$(target) : $(all_objects)\n'
link_text       = '\t$(CXX) $^ -o ../bin/$@ $(LDFLAGS) $(LIBS)\n'

all_obejcts_rule = '$(all_objects) : $(sources)\n'

makedirs_action = ''
makedirs_rule = '\
.PHONY : makedirs\n\
makedirs :\n'
if platform.system() == 'Windows':
	makedirs_action = '\t-mkdir ..\\bin ..\\objects\n'
else:
	makedirs_action = '\t-mkdir ../bin ../objects\n'
makedirs_rule = makedirs_rule + makedirs_action

clean_action = ''
clean_rule = '\
.PHONY : clean\n\
clean : \n'
if platform.system() == 'Windows':
	clean_action = '\t-rmdir /s /q ..\\bin ..\\objects\n'
else:
	clean_action = '\t-rm -rf ../bin ../objects\n'
clean_rule = clean_rule + clean_action

# ============================================================================
# Code
# ============================================================================

# Returns all c++ headers for compiler as headers param.
def get_all_includes(path, added_path, headers):
	files = os.listdir(path)
	for file in files:
		if os.path.isdir(os.path.join(path, file)):
			prefix = ''
			if len(added_path) > 0:
				prefix = added_path + '/'
			get_all_includes(os.path.join(path, file), prefix + file, headers)
		else:
			name = os.path.basename(file)
			prefix = ''
			if len(added_path) > 0:
				prefix = added_path + '/'
			headers.append(prefix + file)

# Returns all c++ headers for compiler as headers param.
def get_current_includes(path, headers):
	files = os.listdir(path)
	for file in files:
		if os.path.isdir(os.path.join(path, file)):
			continue
		else:
			headers.append(file)

# Returns file_list as list with all source files inside '.' catalog.
def get_file_list(path, file_list):
	files = os.listdir(path)
	for file in files:
		if os.path.isdir(os.path.join(path, file)):
			get_file_list(os.path.join(path, file), file_list)
		else:
			if file.endswith(cpp_ext):
				file_list.append(os.path.join(path, file))

# Resturn object list created from src_list
def get_object_filenames(src_list):
	result = []
	for src in src_list:
		result.append(src.replace('.cpp', '.o'))
	return result

# Return dependency from 1 line.
def get_dep_from_line(line):
	start = line.find('"') + 1
	end   = line.rfind('"')
	if start == 0 and end == -1:
		start = line.find('<') + 1
		end   = line.rfind('>')
	return line[start:end]

# Remove file prefix and change separator
def handle_filename(s):
	return replace_separators(remove_file_prefix(s))

# Remove '.\\' preffix from str for Windows and './' for Linux
def remove_file_prefix(s):
	if platform.system() == 'Windows':
		return s[s.find('.\\') + 2:]
	else:
		return s[s.find('./') + 1:]

# Replace '\\' symbol to '/'
# Relevant only for Windows.
def replace_separators(s):
	return s.replace('\\', '/')

# Return if line is valid for check dependency
def is_line_valid(line):
	return line.startswith('#include ')

# Return dependency for one file.
def get_dependency_for_file(filename):
	f = open(filename, 'r')

	object_name = handle_filename(filename)
	deps        = [object_name] # Array with dependencies for file(filename)
	count     = 0  # Counter of lines don't contain '#include' substring
	count_end = 50 # Max count of lines of file we check that don't contain '#include' substring
	for line in f:
		if is_line_valid(line):
			dep = get_dep_from_line(line)
			if dep in headers:
				# Standard c++ header
				continue

			if '..' in dep:
				# File path has relative part that programm can't handle correctly.
				# So we need to remove that part.
				dep = os.path.normpath(os.path.join(os.path.dirname(filename), dep))
				dep = dep.replace('\\', '/')

			if dep.count('/') == 0:
				# File is in root directory.
				# Then add relative path to it's name.
				if line.find('<') == -1:
					if os.path.dirname(filename) != '.':
						dep = remove_file_prefix(os.path.dirname(filename) + '/' + dep)

			# Check existance step
			if not os.path.exists(dep):
				dep = os.path.normpath(os.path.join(os.path.dirname(filename), dep))
				dep = replace_separators(dep)

			deps.append(replace_separators(dep))
		else:
			count += 1
			if count == count_end:
				break

	f.close()
	return deps

# Form all dependencies and return two-demential array as result.
def form_dependencies(file_list):
	deps = [[] for i in range(len(file_list))]
	for i in range(len(file_list)):
		filename = file_list[i]
		deps[i]  = get_dependency_for_file(filename)
	return deps

# Return filename from path.
def get_filename_from_path(filepath):
	return os.path.basename(filepath)

# Form result makefile.
def form_makefile(object_list, deps):
	f = open('Makefile_w', 'w')
	f.write(makefile_text)

	# All objects variable part.
	all_objects = []
	for object_name in object_list:
		object_name = handle_filename(object_name)
		object_name = get_filename_from_path(object_name)
		all_objects.append(objects_dir + object_name)

	f.write('all_objects = \\\n')
	for i in range(len(all_objects) - 1):
		object_name = all_objects[i]
		f.write('\t' + object_name + ' \\\n')
	f.write('\t' + all_objects[len(all_objects) - 1] + '\n\n')

	# Sources part.
	f.write(sources_rule + '\n')

	# Objects' rules part.
	for i, object_rule in enumerate(all_objects):
		f.write(object_rule + ' : ')
		for dep in deps[i]:
			f.write(dep + ' ')
		f.write('\n')
	f.write('\n')

	# All objects rule part.
	f.write(all_obejcts_rule)
	f.write('\t' + compile_text)
	f.write('\n')

	# Linking part.
	f.write(target_dep_text)
	f.write(link_text)
	f.write('\n')

	# In-house rules
	f.write(makedirs_rule + '\n')
	f.write(clean_rule)

	f.close()

# ============================================================================
# Main func
# ============================================================================

# Form C++ headers database.
added_path = ''
for path in headers_path:
	get_all_includes(path, added_path, headers)
for path in dir_headers_path:
	get_current_includes(path, headers)

get_file_list(start_dir, file_list)
if len(file_list) == 0:
	print("There are not any *.cpp or *.h files here.")
	exit(1)
deps = form_dependencies(file_list)
form_makefile(get_object_filenames(file_list), deps)
print("Done!")
